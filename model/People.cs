﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask16.model {
    class People : IEnumerable {

        Person[] people;

        public People(Person[] people) {
            this.people = new Person[people.Length];

            for (int i = 0; i < people.Length; i++) {
                this.people[i] = people[i];
            }
        }

        public IEnumerator GetEnumerator() {
            return new PersonEnum(people);
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }





    }
}

