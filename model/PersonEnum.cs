﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask16.model {
    class PersonEnum : IEnumerator {

        Person[] people;

        int position = -1;


        public PersonEnum(Person[] people) {
            this.people = people;
        }

        public PersonEnum() {
        }

        public bool MoveNext() {
            position++;

            return position < people.Length;   
        }

        public void Reset() {
            position = -1;
        }

        object IEnumerator.Current {
            get {
                return Current;
            }
        }

        public Person Current {
            get {
                try {
                    return people[position];
                }
                catch (IndexOutOfRangeException) {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}
