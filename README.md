**Experis Academy, Norway**

**Authors:**
* **Odd Martin Hansen**

# Task 16: IEnumerable upgrade
Reverse engineer the code on:
* [Microsoft Docs - IEnumerable Interface](https://docs.microsoft.com/en-us/dotnet/api/system.collections.ienumerable?view=netframework-4.8)

Use it to create an enumerable collection for any of you
previous objects created in previous tasks

Demonstrate that it works by running sample LINQ
queries on the collection
